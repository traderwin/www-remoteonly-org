# www-remoteonly-org

This is the source for the https://www.remoteonly.org site.

## Local development

```sh
bundle install

bundle exec middleman
```

Once the Middleman server is running, you can visit `http://localhost:4567/` in
your browser to see a live, local preview of the site. Any changes to files in
the `source` directory will be detected automatically, and your browser will
even reload the page if necessary.

See the [Middleman docs](https://middlemanapp.com/basics/development_cycle/) for
more information.
